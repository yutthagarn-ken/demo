#Build Angular
cd src/main/web
ng build

#Run Spring Boot
cd demo/
mvn spring-boot:run