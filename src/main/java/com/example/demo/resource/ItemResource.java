package com.example.demo.resource;

import com.example.demo.model.Item;
import com.example.demo.repository.ItemRepository;
import com.example.demo.service.ItemService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDateTime;

/**
 * Created by IntelliJ IDEA.
 * User: treepay
 * Date: 19 July 2018
 * Time: 9:34 AM
 **/
@RestController
@RequestMapping("/api/item")
public class ItemResource {

    @Autowired
    private ItemService itemService;

    @Autowired
    private ItemRepository itemRepository;

    @PostMapping("/save")
    public Item insertItem(@RequestBody Item item) {
        item.setCreatedDate(LocalDateTime.now());
        return itemService.insertItem(item);
    }

    @GetMapping
    public Page<Item> getAllItem(Pageable pageable) {
        return itemRepository.findAll(pageable);
    }

}
