package com.example.demo.repository;

import com.example.demo.model.Item;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Created by IntelliJ IDEA.
 * User: treepay
 * Date: 19 July 2018
 * Time: 11:35 AM
 **/
public interface ItemRepository extends JpaRepository<Item, Long> {
}
