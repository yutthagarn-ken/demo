package com.example.demo.service;

import com.example.demo.model.Item;

/**
 * Created by IntelliJ IDEA.
 * User: treepay
 * Date: 19 July 2018
 * Time: 11:40 AM
 **/
public interface ItemService {

    public Item insertItem(Item item);
}
