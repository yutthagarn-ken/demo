package com.example.demo.model;

import lombok.AllArgsConstructor;
import lombok.Data;

import javax.persistence.*;
import java.time.LocalDateTime;

/**
 * Created by IntelliJ IDEA.
 * User: treepay
 * Date: 19 July 2018
 * Time: 9:36 AM
 **/
@Data
@AllArgsConstructor
@Entity
@Table(name = "ITEM")
public class Item {

    @Id
    @GeneratedValue
    private Long id;

    @Column
    private String name;

    private LocalDateTime createdDate;

    public Item() {
    }
}
