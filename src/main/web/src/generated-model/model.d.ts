/* tslint:disable */
// Generated using typescript-generator version 2.5.423 on 2018-09-07 10:22:10.

export interface Page<T> extends Slice<T> {
    totalPages?: number;
    totalElements?: number;
}

export interface Slice<T> extends Streamable<T> {
    size?: number;
    content?: T[];
    number?: number;
    sort?: any;
    numberOfElements?: number;
    first?: boolean;
    last?: boolean;
    pageable?: Pageable;
}

export interface Item {
    id?: number;
    name?: string;
    createdDate?: any;
}

export interface Pageable {
    offset?: number;
    sort?: any;
    paged?: boolean;
    unpaged?: boolean;
    pageNumber?: number;
    pageSize?: number;
}

export interface Streamable<T> {
}
