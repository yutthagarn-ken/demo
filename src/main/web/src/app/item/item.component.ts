import {Component, OnInit} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Item, Page} from "../../generated-model/model";

@Component({
  selector: 'app-item',
  templateUrl: './item.component.html',
  styleUrls: ['./item.component.scss']
})
export class ItemComponent implements OnInit {


  item: Item = {};
  itemList: Page<Item> = {};

  constructor(private http: HttpClient) {
  }

  ngOnInit() {
    this.item.name = '';
    this.listAllItem();
  }

  add() {
    this.http.post('/api/item/save', this.item).subscribe(e => {
      console.log("response data: ", e);
      this.item.name = '';
      this.listAllItem();
    });
  }

  listAllItem() {
    this.http.get<Page<Item>>('/api/item').subscribe(e => {
      this.itemList = e;
    });
  }

}
